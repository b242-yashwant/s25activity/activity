console.log("Jay Shree Ram");

let num1=2;
let getCube=num1**3;
console.log(`The cube of ${num1} is ${getCube}`);

//6. Destructure the array and print out a message with the full address using Template Literals.
 const address=["258","Washington Ave NW", "California", "90011"];
 let [houseNumber,street,state,zipCode] =address;
 console.log(`I live at ${houseNumber} ${street} ${state} ${zipCode}`);
//7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
const animal ={
	name:"LoLong",
	species:"saltwater crocodile",
	weight:"1075 kgs",
	measurement:"20 ft 3 in"
   }

   const {name, species, weight, measurement}=animal;
   console.log(`${name} was a ${species}.He weighted at ${weight} with a measurement of ${measurement}.`);

//8. Destructure the object and print out a message with the details of the animal using Template Literals.



//9. Create an array of numbers.
let numbers = [1,2,3,4,5];

//10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numbers.forEach((num) => {
   console.log(num);
});

//11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
let reduceNumber=numbers.reduce((a,b)=>a+b);
console.log(reduceNumber);



//12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

//13. Create/instantiate a new object from the class Dog and console log the object.
class Dog {
	constructor(name,age, breed){
		this.name=name;
		this.age=age;
		this.breed=breed;
	}
}

const myDog= new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);